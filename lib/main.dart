import 'package:flutter/material.dart';
enum APP_THEME{LIGHT,DARK}
void main() {
  runApp(ContactProfilePage());
}
class MyappTheme{
  static ThemeData appThemeLight(){
    return ThemeData(
      brightness: Brightness.light,
      appBarTheme: AppBarTheme(
        color: Colors.tealAccent,
        iconTheme: IconThemeData(
          color: Colors.black87,
        ),
      ),
      iconTheme: IconThemeData(
        color: Colors.teal,
      ),
    );
  }
  static ThemeData appThemeDark(){
    return ThemeData(
      brightness: Brightness.dark,
      appBarTheme: AppBarTheme(
        color: Colors.tealAccent,
        iconTheme: IconThemeData(
          color: Colors.black87,
        ),
      ),
      iconTheme: IconThemeData(
        color: Colors.orange,
      ),
    );
  }
}
class ContactProfilePage extends StatefulWidget {
  @override
  State<ContactProfilePage> createState() => _ContactProfilePageState();
}

class _ContactProfilePageState extends State<ContactProfilePage> {
  var currentTheme = APP_THEME.LIGHT;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: currentTheme == APP_THEME.DARK
          ? MyappTheme.appThemeLight()
          : MyappTheme.appThemeDark() ,
      home: Scaffold(
        appBar: buildAppbarWidget(),
        body: buliBodyWidget(),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.change_circle),
            onPressed: (){
              setState(() {
                currentTheme == APP_THEME.DARK
                    ?currentTheme = APP_THEME.LIGHT
                    :currentTheme = APP_THEME.DARK;
              });
            },
        ),
      ),
    );
  }
}

Widget buildCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.call,
         // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Call"),
    ],
  );
}

Widget buildTextButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.message,
          //color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("text"),
    ],
  );
}

Widget buildVideoCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.video_call,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Video"),
    ],
  );
}

Widget buildEmailButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.email,
          //  color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Email"),
    ],
  );
}

Widget buildDirectionsButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.directions,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Direction"),
    ],
  );
}

Widget buildPayButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.attach_money,
          //  color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Pay"),
    ],
  );
}

Widget mobilePhoneListTile() {
  return ListTile(
    leading: Icon(Icons.call),
    title: Text("330-802-3365"),
    subtitle: Text("mobile"),
    trailing: IconButton(
      icon: Icon(Icons.message),
      color: Colors.teal,
      onPressed: () {},
    ),
  );
}

Widget otherPhoneListTile() {
  return ListTile(
    leading: Text(''),
    title: Text("440-440-3365"),
    subtitle: Text("other"),
    trailing: IconButton(
      icon: Icon(Icons.message),
      color: Colors.teal,
      onPressed: () {},
    ),
  );
}

Widget emailListTile() {
  return ListTile(
    leading: Icon(Icons.email),
    title: Text("DomeMetal@hotmail.com"),
    subtitle: Text("work"),
  );
}

Widget addressListTile() {
  return ListTile(
    leading: Icon(Icons.location_pin),
    title: Text("012 Sunrise takemehome to the place"),
    subtitle: Text("home"),
    trailing: IconButton(
      icon: Icon(Icons.directions),
      color: Colors.teal,
      onPressed: () {},
    ),
  );
}

Widget buliBodyWidget() {
  return ListView(
    children: <Widget>[
      Column(
        children: <Widget>[
          Container(
            width: double.infinity,

            //Height constraint at Container widget level
            height: 250,

            child: Image.network(
              "https://scontent.fbkk21-1.fna.fbcdn.net/v/t31.18172-8/18451788_644383972429733_2316361255783755800_o.jpg?_nc_cat=111&ccb=1-7&_nc_sid=174925&_nc_eui2=AeHDBXvMGYIzglfvffbu7PK912_kN6u5hP7Xb-Q3q7mE_mvu8R-_lAltU51nTegpzH8FpCikE5xQmkmGRPjArccP&_nc_ohc=Nf-_3sK5F-YAX-vM7qq&_nc_ht=scontent.fbkk21-1.fna&oh=00_AfDsUuGFaoyl3qGv5iKcmKo348Qff5pGDpvArENnLKEH7w&oe=63C9FD6E",
              fit: BoxFit.cover,
            ),
          ),
          Container(
            height: 60,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(8.5),
                  child: Text(
                    "DOME SUNG",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 30),
                  ),
                ),
              ],
            ),
          ),
          Divider(
            color: Colors.black,
          ),
          Container(
            margin: EdgeInsets.only(top: 8, bottom: 8),
            child: Theme(
              data: ThemeData(iconTheme: IconThemeData(color: Colors.pink)),
              child: buildButtonWidget(),
            ),
          ),
          Divider(
            color: Colors.black,
          ),
          mobilePhoneListTile(),
          otherPhoneListTile(),
          Divider(
            color: Colors.black,
          ),
          emailListTile(),
          Divider(
            color: Colors.black,
          ),
          addressListTile(),
        ],
      ),
    ],
  );
}

AppBar buildAppbarWidget() {
  return AppBar(
    backgroundColor: Colors.tealAccent,
    leading: Icon(
      Icons.arrow_back,
      color: Colors.red,
    ),
    actions: <Widget>[
      IconButton(
        onPressed: () {},
        icon: Icon(Icons.star_border),
        color: Colors.red,
      )
    ],
  );
}

Widget buildButtonWidget() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      buildCallButton(),
      buildTextButton(),
      buildVideoCallButton(),
      buildEmailButton(),
      buildDirectionsButton(),
      buildPayButton(),
    ],
  );
}
